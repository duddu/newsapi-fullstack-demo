This project is a little demo of a full-stack journey integrating the News API, using typescript, react+hooks, express, openapi.
The codebase is structured as a monorepo, each package being one part of the stack: server and client.

## Available Scripts

All the commands that interact with the server-side rely on a NEWSAPI_TOKEN environment variable to be populated.

### `yarn start`

Runs both the client and the server in parallel in development mode.<br />
Open [http://localhost:3001](http://localhost:3001) to access the frontend.<br />
Open [http://localhost:3000/api/v1/docs](http://localhost:3000/api/v1/docs) to access the api documentation.

### `yarn test`

Tests in parallel client and server packages using Jest.<br />

### `yarn lint`

Executes in parallel the linting for client and server code.<br />

### `yarn build`

Builds in parallel the frontend and the api.<br />
The output bundles/transpiled code are dropped into a build folder in the respective packages.

### `yarn prod`

Runs just the server, serving the frontend build assets directly on the root path of the node application.<br />
Open [http://localhost:3000](http://localhost:3000/api/v1/docs) to access the frontend.

### `yarn docs`

Generates typescript documentation outputs in the `docs` folder, consumable as static html in the browser.

## TODO

- Complete pagination implementation (e.g. using hateoas and passing a `next` link to the client, with the prepared
  request url to fetch in order to retrieve the next page of results)
- Cancel side effects on ui components unmount
