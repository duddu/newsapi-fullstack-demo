const NewsAPI = require('newsapi')

export interface INewsAPIArticle {
  author: string
  title: string
  description: string
  url: string
  urlToImage: string
  publishedAt: Date
}

export interface INewsAPIQueryResponse {
  status: 'ok' | 'error'
  totalResults: number
  articles: INewsAPIArticle[]
}

export interface INewsAPIQueryRequest {
  q: string
  page: number
}

export interface INewsAPIDomainsRequest {
  domains?: string
  from: Date
  to: Date
}

export interface IArticle extends INewsAPIArticle {
  id: string
}

/**
 * The NewsAPI instance which will be used for any runtime interaction.
 * Instanciating this variable will throw at server start-up in case the
 * environment variable `NEWSAPI_TOKEN` is missing.
 */
export const newsapi = new NewsAPI(process.env.NEWSAPI_TOKEN)

/**
 * Parse the response received from NewsAPI, extracting the relevant payload
 * we want to expose to our consumers.
 * The error handling here is really minimal and covers only the most obvious
 * scenarios; should be improved and expanded to consider this layer valid.
 */
export const parseApiNewsResponse = ({
  status,
  articles,
}: INewsAPIQueryResponse): INewsAPIArticle[] => {
  if (status !== 'ok') {
    throw {
      message: `Unexpected News API status: ${status}`,
    }
  }
  if (!Array.isArray(articles)) {
    throw {
      message: 'Cannot retrieve articles from News API',
    }
  }
  return articles.map(
    ({ author, title, description, url, urlToImage, publishedAt }) => ({
      author,
      title,
      description,
      url,
      urlToImage,
      publishedAt,
    })
  )
}

/**
 * The base request wrapper to communicate with NewsAPI.
 * Accepts all the request payload formats supported by this server.
 */
export const getNewsAPIResponse = async (
  request: INewsAPIQueryRequest | INewsAPIDomainsRequest
) => {
  const response = await newsapi.v2.everything({
    ...request,
    language: 'en',
  })
  return parseApiNewsResponse(response)
}

/**
 * This request search for news based on a query text.
 * @param searchText The keywords text which will be passed as query to NewsAPI
 * @param page The number of the page to retrieve from NewsAPI for this request
 */
export const searchNewsByKeyword = async (searchText: string, page: number) =>
  getNewsAPIResponse({
    q: searchText,
    page,
  })

/**
 * This request search for an article based on the website domain and the time of
 * publishing.
 * @param hostname The hostname of the article to retrieve
 * @param timestamp The time at which the article was published
 */
export const searchNewsByDomainAndTimestamp = async (
  hostname = '',
  timestamp: Date
) =>
  getNewsAPIResponse({
    domains: hostname.replace(/^www\./, ''),
    from: timestamp,
    to: timestamp,
  })
