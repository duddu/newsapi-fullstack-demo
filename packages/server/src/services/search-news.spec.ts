import * as search from './search-news'

jest.mock('newsapi')

describe('[SearchNews service]', () => {
  describe('parseApiNewsResponse()', () => {
    const mockApiNewsResponse: any = {
      status: 'ok',
      totalResults: 1,
      articles: [
        {
          author: 'author-mock',
          title: 'title-mock',
        },
      ],
    }

    it('should throw if status is not ok', () => {
      expect(() =>
        search.parseApiNewsResponse({
          ...mockApiNewsResponse,
          status: 'error',
        })
      ).toThrowError(/status: error/i)
    })

    it('should throw if status is not ok', () => {
      expect(() =>
        search.parseApiNewsResponse({
          ...mockApiNewsResponse,
          articles: null,
        })
      ).toThrowError(/cannot retrieve articles/i)
    })

    it('should return parsed news response', () => {
      const res = search.parseApiNewsResponse(mockApiNewsResponse)
      expect(res).toHaveLength(1)
      expect(res[0]).toMatchObject({
        author: 'author-mock',
        title: 'title-mock',
      })
    })
  })

  describe('[getNewsAPIResponse()', () => {
    afterEach(() => jest.restoreAllMocks())

    it('should call /everything endpoint', async () => {
      const everything = jest.spyOn(search.newsapi.v2, 'everything')
      await search.getNewsAPIResponse({ q: 'query-mock', page: 1 })
      expect(everything).toBeCalledWith({
        q: 'query-mock',
        page: 1,
        language: 'en',
      })
    })

    it('should invoke parseApiNewsResponse', async () => {
      const parse = jest.spyOn(search, 'parseApiNewsResponse')
      await search.getNewsAPIResponse({ q: 'query-mock', page: 1 })
      expect(parse).toBeCalled()
    })
  })

  describe('[searchNewsByKeyword()', () => {
    afterEach(() => jest.restoreAllMocks())

    it('should invoke getNewsAPIResponse', async () => {
      const getNews = jest.spyOn(search, 'getNewsAPIResponse')
      await search.searchNewsByKeyword('query-mock', 1)
      expect(getNews).toBeCalledWith({ q: 'query-mock', page: 1 })
    })
  })

  describe('[searchNewsByDomainAndTimestamp()', () => {
    afterEach(() => jest.restoreAllMocks())

    it('should invoke getNewsAPIResponse', async () => {
      const getNews = jest.spyOn(search, 'getNewsAPIResponse')
      const now = new Date()
      await search.searchNewsByDomainAndTimestamp('www.host.com', now)
      expect(getNews).toBeCalledWith({
        domains: 'host.com',
        from: now,
        to: now,
      })
    })

    it('should invoke getNewsAPIResponse with empty domain', async () => {
      const getNews = jest.spyOn(search, 'getNewsAPIResponse')
      const now = new Date()
      await search.searchNewsByDomainAndTimestamp(undefined, now)
      expect(getNews).toBeCalledWith({ domains: '', from: now, to: now })
    })
  })

  describe('newsapi', () => {
    it('should be an instance of NewsAPI', () => {
      expect(search.newsapi).toBeInstanceOf(require('newsapi'))
    })
  })
})
