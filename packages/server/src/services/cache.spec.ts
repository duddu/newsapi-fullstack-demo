import * as NodeCache from 'node-cache'

import { newsResponseCache, newsUuidMapCache } from './cache'

describe('[Cache service]', () => {
  describe('[newsResponseCache]', () => {
    it('should be an instance of NodeCache', () => {
      expect(newsResponseCache).toBeInstanceOf(NodeCache)
    })
  })

  describe('[newsUuidMapCache]', () => {
    it('should be an instance of NodeCache', () => {
      expect(newsUuidMapCache).toBeInstanceOf(NodeCache)
    })
  })
})
