import * as NodeCache from 'node-cache'

import { IArticle, INewsAPIArticle } from './search-news'

export type NewsResponseCacheItem = INewsAPIArticle[]

export type NewsUuidMapCacheItem = Pick<IArticle, 'url' | 'publishedAt'>

/**
 * Short-lived cache meant to persist the reecent responses from NewsAPI, keyd by the request query.
 * This is NOT opmitised for production, and is just a quick stub to show how server performances
 * could be improved.
 */
export const newsResponseCache = new NodeCache({
  stdTTL: 60 * 3,
})

/**
 * Long-lived cache meant to persist the mapping between a uuid and the `url` and `publishedAt`
 * for a specific article fetched from NewsAPI.
 * This allows us to re-fetch the same article later on using the keys of this cache, by being
 * able to compose a [[INewsAPIDomainsRequest]] starting from the persisted information.
 * This is NOT opmitised for production, and is just a quick stub to show how server performances
 * could be improved.
 */
export const newsUuidMapCache = new NodeCache({
  stdTTL: 60 * 60,
})
