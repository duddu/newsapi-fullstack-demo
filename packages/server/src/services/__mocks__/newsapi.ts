class NewsAPIMock {
  v2 = {
    everything: jest.fn(() => Promise.resolve({
      status: 'ok',
      articles: []
    }))
  }
}

module.exports = NewsAPIMock
