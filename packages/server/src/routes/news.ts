import { getArticles as GET } from '../controllers/news'

const operations = {
  GET
}

export default () => {
  GET.apiDoc = {
    summary: 'Returns paginated news filtered by keywords.',
    operationId: 'getArticles',
    parameters: [
      {
        in: 'query',
        name: 'search',
        required: true,
        type: 'string'
      },
      {
        in: 'query',
        name: 'page',
        required: false,
        type: 'integer'
      }
    ],
    responses: {
      200: {
        description: 'List of the news matching the search criteria.',
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/Article'
          }
        }
      },
      default: {
        description: 'Server error',
        schema: {
          additionalProperties: true
        }
      }
    }
  }

  return operations
}
