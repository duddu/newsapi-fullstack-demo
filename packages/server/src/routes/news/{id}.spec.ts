import route from './{id}'

describe('[News/:id route]', () => {
  const news = route()

  describe('[GET]', () => {
    it('Operation should be defined', () => {
      expect(news).toHaveProperty('GET')
      expect(news.GET).toBeDefined()
    })

    it('Api should document getArticle operation', () => {
      expect(news.GET.apiDoc).toHaveProperty('operationId', 'getArticle')
    })
  })
})
