import { getArticle as GET } from '../../controllers/news'

const operations = {
  GET
}

export default () => {
  GET.apiDoc = {
    summary: 'Returns a single article by ID.',
    operationId: 'getArticle',
    parameters: [
      {
        in: 'path',
        name: 'id',
        required: true,
        type: 'string',
        format: 'uuid'
      }
    ],
    responses: {
      200: {
        description: 'The article matching the internal uuid.',
        schema: {
          $ref: '#/definitions/Article'
        }
      },
      default: {
        description: 'Server error',
        schema: {
          additionalProperties: true
        }
      }
    }
  }

  return operations
}
