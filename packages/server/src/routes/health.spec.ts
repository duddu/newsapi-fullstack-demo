import route from './health'

describe('[Health route]', () => {
  const health = route()

  describe('[GET]', () => {
    it('Operation should be defined', () => {
      expect(health).toHaveProperty('GET')
      expect(health.GET).toBeDefined()
    })

    it('Api should document getHealth operation', () => {
      expect(health.GET.apiDoc).toHaveProperty('operationId', 'getHealth')
    })
  })
})
