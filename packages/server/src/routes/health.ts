import { getHealth as GET } from '../controllers/health'

const operations = {
  GET
}

export default () => {
  GET.apiDoc = {
    summary: 'Returns the health of the api.',
    operationId: 'getHealth',
    responses: {
      200: {
        description:
          'A (completely naive) health status report of the service.',
        schema: {
          $ref: '#/definitions/Health'
        }
      },
      default: {
        description: 'Server error',
        schema: {
          additionalProperties: true
        }
      }
    }
  }

  return operations
}
