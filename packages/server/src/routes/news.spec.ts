import route from './news'

describe('[News route]', () => {
  const news = route()

  describe('[GET]', () => {
    it('Operation should be defined', () => {
      expect(news).toHaveProperty('GET')
      expect(news.GET).toBeDefined()
    })

    it('Api should document getArticles operation', () => {
      expect(news.GET.apiDoc).toHaveProperty('operationId', 'getArticles')
    })
  })
})
