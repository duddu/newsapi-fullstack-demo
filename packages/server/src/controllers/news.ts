import * as express from 'express'
import * as url from 'url'
import { v4 as uuid } from 'uuid'
import { OperationFunction } from 'express-openapi'

import {
  NewsResponseCacheItem,
  NewsUuidMapCacheItem,
  newsResponseCache,
  newsUuidMapCache,
} from '../services/cache'
import {
  searchNewsByKeyword,
  searchNewsByDomainAndTimestamp,
} from '../services/search-news'

/**
 * Get the list of articles available for a specific request query:
 * - If an identical request to NewsAPI was recently fulfilled, return the cached response
 * - Otherwise, fetch the fresh articles from NewsAPI based on the request query, and then
 * - Assign a uuid to each of the fetched articles
 * - Persist in cache a mapping between that uuid and the url and publidhed date of the article
 * - Persist in cache the list of articles retrieved, keyd by the request
 */
export const getArticles: OperationFunction = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
): Promise<express.Response | void> => {
  const { search, page = 1 } = req.query
  const cacheKey = `${search}#${page}`
  const cache = newsResponseCache.get<NewsResponseCacheItem>(cacheKey)
  if (cache) {
    return res.status(200).json(cache)
  }
  try {
    let news = await searchNewsByKeyword(search, page)
    news = news.map(item => {
      const id = uuid()
      newsUuidMapCache.set<NewsUuidMapCacheItem>(id, {
        url: item.url,
        publishedAt: item.publishedAt,
      })
      return {
        ...item,
        id,
      }
    })
    newsResponseCache.set<NewsResponseCacheItem>(cacheKey, news)
    return res.status(200).json(news)
  } catch (e) {
    return next(e)
  }
}

/**
 * Get a single article by id, leveraging the information saved in cache:
 * - Check if the id in the request is present in the server cache
 * - Get the article url and publish datestamp from the cache
 * - Extract the hostmane from the article full url
 * - Fetch articles from NewsApi based on those hostname and timestamp
 * - Even if the result is likely to be just one, return from the response
 *   the article which matches the cached url
 * Perfectly aware on how much error prone this caching strategy is, this
 * endpoint is just meant to serve a possible solution to client deeplinking.
 */
export const getArticle: OperationFunction = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
): Promise<express.Response | void> => {
  const { id } = req.params
  const cache = newsUuidMapCache.get<NewsUuidMapCacheItem>(id)
  if (!cache) {
    return res.status(404).json({
      message: 'No cache available',
      id,
    })
  }
  try {
    const { hostname } = url.parse(cache.url)
    const news = await searchNewsByDomainAndTimestamp(
      hostname,
      cache.publishedAt
    )
    const article = news.find(item => item.url === cache.url)
    if (!article) {
      return res.status(404).json({
        message: 'Cannot retrieve article',
        url: cache.url,
        id,
      })
    }
    return res.status(200).json({
      ...article,
      id,
    })
  } catch (e) {
    return next(e)
  }
}
