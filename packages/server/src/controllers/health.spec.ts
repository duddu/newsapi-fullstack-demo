import { getHealth } from './health'

jest.mock('express')

const mockRequest = require('mock-express-request')
const mockResponse = require('mock-express-response')

describe('[Health controller]', () => {
  afterEach(() => jest.restoreAllMocks())

  describe('[getHealth()]', () => {
    it('should return positive health status', () => {
      const res = new mockResponse()
      const resStatus = jest.spyOn(res, 'status')
      const resJson = jest.spyOn(res, 'json')
      const nextFn = jest.fn()
      getHealth(new mockRequest({}), res, nextFn)
      expect(resStatus).toBeCalledWith(200)
      expect(resJson).toBeCalledWith({
        health: true
      })
      expect(nextFn).not.toHaveBeenCalled()
    })
  })
})
