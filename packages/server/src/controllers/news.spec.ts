import { getArticles, getArticle } from './news'
import * as caches from '../services/cache'
import * as search from '../services/search-news'

const mockRequest = require('mock-express-request')
const mockResponse = require('mock-express-response')

jest.mock('express')
jest.mock('uuid')

describe('[News controller]', () => {
  const getResponseSpy = () => {
    const res = new mockResponse()
    return {
      res,
      status: jest.spyOn(res, 'status'),
      json: jest.spyOn(res, 'json'),
      next: jest.fn()
    }
  }

  afterEach(() => jest.restoreAllMocks())

  describe('[getArticles()]', () => {
    const req = new mockRequest({
      query: {
        search: 'search-mock',
        page: 7
      }
    })

    it('should return cached response if present', async () => {
      const { res, status, json, next } = getResponseSpy()
      const responseCache = jest.spyOn(caches.newsResponseCache, 'get')
      responseCache.mockReturnValue({ cache: 'hit' })
      await getArticles(req, res, next)
      expect(responseCache).toBeCalledWith('search-mock#7')
      expect(status).toBeCalledWith(200)
      expect(json).toBeCalledWith({ cache: 'hit' })
      expect(next).not.toHaveBeenCalled()
    })

    it('should return cached response if present', async () => {
      const { res, status, json, next } = getResponseSpy()
      const getNews = jest.spyOn(search, 'searchNewsByKeyword')
      const uuidMapCache = jest.spyOn(caches.newsUuidMapCache, 'set')
      const responseCache = jest.spyOn(caches.newsResponseCache, 'set')
      const uuid = require('uuid')
      const getId = jest.spyOn(uuid, 'v4')
      getNews.mockReturnValue(
        Promise.resolve([
          {
            title: 'title-mock',
            url: 'url-mock',
            publishedAt: 'date-mock'
          }
        ] as any)
      )
      getId.mockReturnValue('uuid-mock')
      await getArticles(req, res, next)
      expect(getNews).toBeCalledWith('search-mock', 7)
      expect(uuidMapCache).toBeCalledWith('uuid-mock', {
        url: 'url-mock',
        publishedAt: 'date-mock'
      })
      expect(responseCache).toBeCalledWith('search-mock#7', [
        {
          id: 'uuid-mock',
          title: 'title-mock',
          url: 'url-mock',
          publishedAt: 'date-mock'
        }
      ])
      expect(status).toBeCalledWith(200)
      expect(json).toBeCalledWith([
        {
          id: 'uuid-mock',
          title: 'title-mock',
          url: 'url-mock',
          publishedAt: 'date-mock'
        }
      ])
      expect(next).not.toHaveBeenCalled()
    })

    it('should sent exceptions to error middleware', async () => {
      const { res, status, json, next } = getResponseSpy()
      const req = new mockRequest()
      const getNews = jest.spyOn(search, 'searchNewsByKeyword')
      getNews.mockReturnValue(Promise.reject(new Error('err-mock')))
      await getArticles(req, res, next)
      expect(status).not.toHaveBeenCalled()
      expect(json).not.toHaveBeenCalled()
      expect(next).toBeCalledWith(new Error('err-mock'))
    })
  })

  describe('[getArticle()]', () => {
    const req = new mockRequest({
      params: {
        id: 'uuid-mock'
      }
    })

    it('should return 404 if cache key is not found', async () => {
      const { res, status, json, next } = getResponseSpy()
      const uuidMapCache = jest.spyOn(caches.newsUuidMapCache, 'get')
      uuidMapCache.mockReturnValue(null)
      await getArticle(req, res, next)
      expect(uuidMapCache).toHaveBeenCalledWith('uuid-mock')
      expect(status).toHaveBeenCalledWith(404)
      expect(json).toHaveBeenCalledWith({
        message: expect.any(String),
        id: 'uuid-mock'
      })
      expect(next).not.toHaveBeenCalled()
    })

    it('should return 404 if the original article is not found', async () => {
      const { res, status, json, next } = getResponseSpy()
      const getNews = jest.spyOn(search, 'searchNewsByDomainAndTimestamp')
      getNews.mockReturnValue(Promise.resolve([]))
      await getArticle(req, res, next)
      expect(status).toHaveBeenCalledWith(404)
      expect(json).toHaveBeenCalledWith({
        message: expect.any(String),
        url: expect.any(String),
        id: 'uuid-mock'
      })
      expect(next).not.toHaveBeenCalled()
    })

    it('should retrieve original article from cached reference', async () => {
      const { res, status, json, next } = getResponseSpy()
      const uuidMapCache = jest.spyOn(caches.newsUuidMapCache, 'get')
      uuidMapCache.mockReturnValue({ url: 'url-mock' })
      const getNews = jest.spyOn(search, 'searchNewsByDomainAndTimestamp')
      getNews.mockReturnValue(Promise.resolve([{ url: 'url-mock' } as any]))
      const uuid = require('uuid')
      const getId = jest.spyOn(uuid, 'v4')
      getId.mockReturnValue('uuid-mock')
      await getArticle(req, res, next)
      expect(status).toHaveBeenCalledWith(200)
      expect(json).toHaveBeenCalledWith({
        url: 'url-mock',
        id: 'uuid-mock'
      })
      expect(next).not.toHaveBeenCalled()
    })

    it('should sent exceptions to error middleware', async () => {
      const { res, status, json, next } = getResponseSpy()
      const getNews = jest.spyOn(search, 'searchNewsByDomainAndTimestamp')
      getNews.mockReturnValue(Promise.reject(new Error('err-mock')))
      await getArticle(req, res, next)
      expect(status).not.toHaveBeenCalled()
      expect(json).not.toHaveBeenCalled()
      expect(next).toBeCalledWith(new Error('err-mock'))
    })
  })
})
