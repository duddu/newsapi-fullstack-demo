import { OperationFunction } from 'express-openapi'

/**
 * An extremely naive health controller.
 * Ideally, this controller should also check the health of the providers
 * which are vital to the funcioning og thi server (e.g. NewsApi in this case),
 * and report about the availability of those.
 */
export const getHealth: OperationFunction = (req, res) => {
  res.status(200).json({
    health: true,
  })
}
