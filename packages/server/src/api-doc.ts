import { OpenAPIV2 } from 'openapi-types'

const apiDoc: OpenAPIV2.Document = {
  swagger: '2.0',
  basePath: '/api/v1',
  info: {
    title: 'An API to search for news.',
    version: '1.0.0'
  },
  produces: ['application/json'],
  definitions: {
    Health: {
      type: 'object',
      properties: {
        health: {
          description: 'A boolean indicating the health of the service.',
          type: 'boolean'
        }
      },
      required: ['health']
    },
    Article: {
      type: 'object',
      properties: {
        author: {
          description: 'The author of the article.',
          type: 'string'
        },
        title: {
          description: 'The title of the article.',
          type: 'string'
        },
        description: {
          description: 'The initial sentences of the article.',
          type: 'string'
        },
        url: {
          description: 'The public URL of the article.',
          type: 'string'
        },
        urlToImage: {
          description: 'The URL of the image associated with the article.',
          type: 'string'
        },
        publishedAt: {
          description: 'The time at which the article has been published.',
          type: 'string',
          format: 'date-time'
        },
        id: {
          description: 'The internal unique identifier of the article.',
          type: 'string',
          format: 'uuid'
        }
      },
      required: [
        'author',
        'title',
        'description',
        'url',
        'urlToImage',
        'publishedAt',
        'id'
      ]
    }
  },
  paths: {}
}

export default apiDoc
