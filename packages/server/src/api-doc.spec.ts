import apiDoc from './api-doc'

describe('[ApiDoc]', () => {
  it('should be an object', () => {
    expect(apiDoc).toBeInstanceOf(Object)
  })

  it('should implement swagger v2 spec', () => {
    expect(apiDoc.swagger).toStrictEqual('2.0')
  })

  it('should produce json content', () => {
    expect(apiDoc.produces).toHaveLength(1)
    expect((apiDoc.produces as string[])[0]).toStrictEqual('application/json')
  })

  it('should not default any path', () => {
    expect(apiDoc.paths).toStrictEqual({})
  })
})
