import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as express from 'express'
import * as path from 'path'
import { initialize } from 'express-openapi'

import v1ApiDoc from './api-doc'

const port = 3000

const app = express()
  .disable('x-powered-by')
  .use(cors())
  .use(bodyParser.json())

// Initialise openapi middleware
initialize({
  app,
  apiDoc: v1ApiDoc,
  docsPath: '/docs',
  paths: path.resolve(__dirname, 'routes'),
  routesGlob: '**/*.{ts,js}',
  routesIndexFileRegExp: /(?:index)?\.[tj]s$/,
  pathsIgnore: new RegExp('.(spec|test)$'),
  errorMiddleware: (err, req, res, next) => {
    console.error('Server error', err)
    res.status(err.status || 500).send(err)
  },
})

const onListen = () => {
  console.log(`Server listening on port ${port}`)
}

const onError = (error: any) => {
  if (error.syscall === 'listen') {
    // Handle known errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(`Binding [${port}] requires elevated privileges`)
        break
      case 'EADDRINUSE':
        console.error(`Port [${port}] is already in use`)
        break
      default:
        console.error('Unknown listen error', error)
    }
  } else {
    console.error('Unknown server error:', error)
  }
  process.exit(1)
}

// When running in production mode, serve the frontend bundles as static
// assets, and direct all the uncaught requests to the index.html
if (process.env.NODE_ENV === 'production') {
  const client = path.join(__dirname, '../../client/build')
  app.use(express.static(client))
  app.use('/*', (req, res) => {
    res.sendFile(path.join(client, 'index.html'))
  })
}

app
  .listen(port)
  .on('listening', onListen)
  .on('error', onError)
