import React from 'react'
import { render, waitForDomChange } from '@testing-library/react'

import App from './App'

describe('[App]', () => {
  it('should render without crashing', async () => {
    const { unmount } = render(<App />)
    await waitForDomChange()
    expect(unmount()).toBe(true)
  })
})
