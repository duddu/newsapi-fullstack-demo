import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { ArticlesList } from './news/ArticlesList'
import { Article } from './news/Article'
import Provider from './news/Provider'

const App: React.FC = () => {
  return (
    <Router>
      <Provider>
        <Switch>
          <Route exact path="/">
            <ArticlesList />
          </Route>
        </Switch>
        <Switch>
          <Route path="/:newsId">
            <Article />
          </Route>
        </Switch>
      </Provider>
    </Router>
  )
}

export default App
