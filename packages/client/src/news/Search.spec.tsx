import React, { useContext } from 'react'
import { act, cleanup, fireEvent, render, wait } from '@testing-library/react'

import Provider, { Context } from './Provider'
import { Search, initialSearchText } from './Search'

jest.mock('./request')

const TestSearchEffect: React.FC = () => {
  const { search } = useContext(Context)
  return (
    <div data-testid='test-search-effect'>
      {search.text}
    </div>
  )
}

afterEach(cleanup)

describe('[Search]', () => {
  const search = <Provider><Search /><TestSearchEffect /></Provider>

  it('should render without crashing', async () => {
    const { unmount } = render(search)
    await wait(() => expect(unmount()).toBe(true))
  })

  it('input field should have initial value', async () => {
    const { getByPlaceholderText } = render(search)
    await wait(() =>
      expect(getByPlaceholderText(/search/i).getAttribute('value')).toBe(initialSearchText))
  })

  it('form submit should update the context state', async () => {
    const { getByTestId } = render(search)
    await wait(() =>
      expect(getByTestId('test-search-effect').textContent).toBe(initialSearchText))
    act(() => {
      fireEvent.change(getByTestId('search'), {target: {value: 'new search'}})
    })
    act(() => {
      fireEvent.submit(getByTestId('form'))
    })
    await wait(() => expect(getByTestId('test-search-effect').textContent).toBe('new search'))
  })
})
