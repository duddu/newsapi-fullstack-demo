import React from 'react'
import { cleanup, render, waitForDomChange } from '@testing-library/react'

import { Feedback } from './Feedback'
import { Search } from './Search'
import Provider from './Provider'

jest.mock('./request', () => ({
  fetchArticles: jest.fn(),
  parseServerError: () => ({
    message: 'server-error-mock'
  })
}))

afterEach(() => {
  jest.restoreAllMocks()
  cleanup()
})

describe('[Feedback]', () => {
  const feedbackWithContext = <Provider><Search /><Feedback /></Provider>

  it('should render without crashing', () => {
    const { unmount } = render(<Feedback />)
    expect(unmount()).toBe(true)
  })

  it('should be empty if no context provided', () => {
    const { container } = render(<Feedback />)
    expect(container.textContent).toBe('')
  })

  it('should render not found error message', async () => {
    require('./request').fetchArticles.mockReturnValue(
      Promise.resolve({
        data: []
      })
    )
    const { getByTestId } = render(feedbackWithContext)
    await waitForDomChange()
    expect(getByTestId('empty').textContent).toMatch(/no news found/i)
  })

  it('should render server error message', async () => {
    require('./request').fetchArticles.mockReturnValue(
      Promise.reject()
    )
    const { getByTestId } = render(feedbackWithContext)
    await waitForDomChange()
    expect(getByTestId('error').textContent).toBe('server-error-mock')
  })

  it('should render loading spinner', async () => {
    require('./request').fetchArticles.mockReturnValue(
      Promise.resolve({})
    )
    const { findByTestId } = render(feedbackWithContext)
    await expect(findByTestId('loading')).resolves.toBeDefined()
  })
})
