import React, { useContext } from 'react'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid'
import ErrorIcon from '@material-ui/icons/Error'
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied'
import Typography from '@material-ui/core/Typography'

import { Context } from './Provider'

const useStyles = makeStyles(() =>
  createStyles({
    progress: {
      textAlign: 'center',
      margin: 30,
    },
    icon: {
      marginTop: 30,
      textAlign: 'center',
    },
    message: {
      marginTop: 15,
      textAlign: 'center',
    },
  })
)

/**
 * Displays user feedback to inform of loading and errors statuses
 */
export const Feedback: React.FC = () => {
  const classes = useStyles()

  const { news, isLoading, error } = useContext(Context)

  return (
    <React.Fragment>
      {isLoading && (
        <Grid item xs={12} className={classes.progress}>
          <CircularProgress data-testid="loading" />
        </Grid>
      )}
      {!isLoading && error && (
        <React.Fragment>
          <Grid item xs={12} className={classes.icon}>
            <ErrorIcon color="error" fontSize="large" />
          </Grid>
          <Grid item xs={12} className={classes.message}>
            <Typography gutterBottom component="p" data-testid="error">
              {error.message || 'Whoops...'}
            </Typography>
          </Grid>
        </React.Fragment>
      )}
      {Array.isArray(news) && !news.length && !isLoading && !error && (
        <React.Fragment>
          <Grid item xs={12} className={classes.icon}>
            <SentimentDissatisfiedIcon color="primary" fontSize="large" />
          </Grid>
          <Grid item xs={12} className={classes.message}>
            <Typography gutterBottom component="p" data-testid="empty">
              No news found!
            </Typography>
          </Grid>
        </React.Fragment>
      )}
    </React.Fragment>
  )
}
