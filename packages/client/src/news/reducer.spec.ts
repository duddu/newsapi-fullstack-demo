import * as actions from './actions'
import { initialState, reducer } from './reducer'

describe('[reducer]', () => {
  describe('initialState', () => {
    it('should return the default state', () => {
      expect(initialState).toStrictEqual({
        search: {},
        isLoading: false,
        news: null,
        error: null,
      })
    })
  })

  describe('searchArticles()', () => {
    it('should set the search text value', () => {
      expect(reducer(initialState, actions.searchArticles('text'))).toEqual({
        ...initialState,
        isLoading: true,
        search: { text: 'text' },
      })
    })

    it('should skip if no search text change', () => {
      const initialWithSearch = {
        ...initialState,
        search: { text: 'text2' }
      }
      expect(reducer(initialWithSearch, actions.searchArticles('text2'))).toEqual(initialWithSearch)
    })
  })

  describe('searchArticlesSuccess()', () => {
    it('should set the news results', () => {
      expect(
        reducer(initialState, actions.searchArticlesSuccess(['article'] as any))
      ).toEqual({
        ...initialState,
        news: ['article'],
      })
    })
  })

  describe('searchArticlesFailure()', () => {
    it('should set the error object', () => {
      expect(
        reducer(
          initialState,
          actions.searchArticlesFailure({
            message: 'error',
          })
        )
      ).toEqual({
        ...initialState,
        error: { message: 'error' },
      })
    })
  })

  describe('getArticleById()', () => {
    it('should set the search id value', () => {
      expect(reducer(initialState, actions.getArticleById('uuid'))).toEqual({
        ...initialState,
        isLoading: true,
        search: { id: 'uuid' },
      })
    })
  })

  describe('getArticleByIdSuccess()', () => {
    it('should set the news result', () => {
      expect(
        reducer(initialState, actions.getArticleByIdSuccess('article' as any))
      ).toEqual({
        ...initialState,
        news: ['article'],
      })
    })

    it('should concat article result with existing news', () => {
      expect(
        reducer(
          {
            ...initialState,
            news: ['article1' as any],
          },
          actions.getArticleByIdSuccess('article2' as any)
        )
      ).toEqual({
        ...initialState,
        news: ['article1', 'article2'],
      })
    })
  })

  describe('getArticleByIdFailure()', () => {
    it('should set the error object', () => {
      expect(
        reducer(
          initialState,
          actions.getArticleByIdFailure({
            message: 'error',
          })
        )
      ).toEqual({
        ...initialState,
        error: { message: 'error' },
      })
    })
  })

  describe('loadMoreArticles()', () => {
    it('should set the search page', () => {
      expect(reducer(initialState, actions.loadMoreArticles(3))).toEqual({
        ...initialState,
        isLoading: true,
        search: { page: 3 },
      })
    })
  })

  describe('loadMoreArticlesSuccess()', () => {
    it('should set the news result', () => {
      expect(
        reducer(
          initialState,
          actions.loadMoreArticlesSuccess(['article'] as any)
        )
      ).toEqual({
        ...initialState,
        news: ['article'],
      })
    })

    it('should concat articles results with existing news', () => {
      expect(
        reducer(
          {
            ...initialState,
            news: ['article1' as any],
          },
          actions.loadMoreArticlesSuccess(['article2'] as any)
        )
      ).toEqual({
        ...initialState,
        news: ['article1', 'article2'],
      })
    })
  })

  describe('loadMoreArticlesFailure()', () => {
    it('should set the error object', () => {
      expect(
        reducer(
          initialState,
          actions.loadMoreArticlesFailure({
            message: 'error',
          })
        )
      ).toEqual({
        ...initialState,
        error: { message: 'error' },
      })
    })
  })

  describe('resetSearchResults()', () => {
    it('should reset the news result', () => {
      expect(
        reducer(
          {
            ...initialState,
            news: ['article' as any],
          },
          actions.resetSearchResults()
        )
      ).toEqual({
        ...initialState,
        news: [],
      })
    })
  })
})
