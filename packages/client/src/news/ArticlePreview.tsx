import React from 'react'
import { Link } from 'react-router-dom'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'

import { IArticle } from 'server/src/services/search-news'

const useStyles = makeStyles(() =>
  createStyles({
    title: {
      fontSize: 14,
    },
    media: {
      height: 140,
    },
    link: {
      textDecoration: 'none',
      color: 'black',
    },
  })
)

export const getHostname = (url: string) => {
  try {
    return new URL(url).hostname
  } catch (e) {
    return null
  }
}

/**
 * Displays the preview of an article from props received
 */
export const ArticlePreview: React.FC<IArticle> = ({
  id,
  title,
  url,
  urlToImage,
}) => {
  const classes = useStyles()

  return (
    <Card>
      <Link to={`/${id}`} className={classes.link} data-testid="link">
        <CardActionArea>
          {urlToImage && (
            <CardMedia
              className={classes.media}
              image={urlToImage}
              title={title}
            />
          )}
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              data-testid="title"
            >
              {title}
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              component="p"
              data-testid="hostname"
            >
              {getHostname(url)}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>
    </Card>
  )
}
