import axios, { AxiosPromise } from 'axios'

import { IArticle } from 'server/src/services/search-news'

const baseRequest = axios.create({
  baseURL: 'http://localhost:3000/api/v1/news',
  responseType: 'json',
  timeout: 10000,
})

/**
 * Fetch articles based on a search text
 */
export const fetchArticles = (search: string): AxiosPromise<IArticle[]> =>
  baseRequest.request({
    params: {
      search,
    },
  })

/**
 * Fetch a single article by id
 */
export const fetchArticle = (id: string): AxiosPromise<IArticle> =>
  baseRequest.get(`/${id}`)

export const parseServerError = ({
  response,
  message,
}: any): { message: string } => ({
  message: (response && response.data && response.data.message) || message,
})
