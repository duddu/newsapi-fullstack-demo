import React, { useContext } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import { ArticlePreview } from './ArticlePreview'
import { Feedback } from './Feedback'
import { Context } from './Provider'
import { Search } from './Search'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      padding: 15,
    },
  }),
);

/**
 * Displays the list of articles found and a search form
 */
export const ArticlesList: React.FC = () => {
  const classes = useStyles()
  const { news } = useContext(Context)

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Search />
        </Grid>
        <Feedback />
        {news &&
          news.map(article =>
            <Grid item xs={12} sm={6} md={4} lg={3} key={article.id}>
              <ArticlePreview {...article} />
            </Grid>)}
      </Grid>
    </div>
  )
}
