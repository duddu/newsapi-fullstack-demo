import React, { useReducer, useEffect } from 'react'

import {
  getArticleByIdFailure,
  getArticleByIdSuccess,
  INewsActionPayload,
  resetSearchResults,
  searchArticlesFailure,
  searchArticlesSuccess,
} from './actions'
import { INewsState, initialState, reducer } from './reducer'
import { fetchArticles, fetchArticle, parseServerError } from './request'

interface INewsContext extends INewsState {
  dispatch: React.Dispatch<INewsActionPayload>
}

export const Context = React.createContext({} as INewsContext)

const Provider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  const { news, isLoading, error, search } = state

  // This effect fetch articles every time the context search
  // state value is updated by any consumer, and updates the state
  // accordingly after the request is completed
  useEffect(() => {
    if (!search.text) return
    // Clear the current results from the state, to avoid displaying
    // an article fetched from the detail page in case of deeplinking
    dispatch(resetSearchResults())
    fetchArticles(search.text)
      .then(({ data }) => dispatch(searchArticlesSuccess(data)))
      .catch(error =>
        dispatch(
          searchArticlesFailure(parseServerError(error))
        )
      )
  }, [search.text])

  // useEffect(() => {
  //   if (!search.page) return
  //   fetch(`${apiBaseUrl}?search=${search.text}&page=${search.page}`)
  //     .then(response => response.json())
  //     .then(data => dispatch(loadMoreArticlesSuccess(data)))
  //     .then(data => dispatch(loadMoreArticlesFailure(data)))
  // }, [search.page])

  // This effect fetch a single article every time the context search
  // state value is updated by any consumer, and updates the state
  // accordingly after the request is completed
  useEffect(() => {
    if (!search.id) return
    fetchArticle(search.id)
      .then(({ data }) => dispatch(getArticleByIdSuccess(data)))
      .catch(error =>
        dispatch(
          getArticleByIdFailure(parseServerError(error))
        )
      )
  }, [search.id])

  return (
    <Context.Provider value={{ news, isLoading, error, search, dispatch }}>
      {children}
    </Context.Provider>
  )
}

export default Provider
