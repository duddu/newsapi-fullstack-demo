import { IArticle } from 'server/src/services/search-news'

export enum NewsActions {
  SEARCH_ARTICLES = 'SEARCH_ARTICLES',
  SEARCH_ARTICLES_SUCCESS = 'SEARCH_ARTICLES_SUCCESS',
  SEARCH_ARTICLES_FAILURE = 'SEARCH_ARTICLES_FAILURE',
  LOAD_MORE_ARTICLES = 'LOAD_MORE_ARTICLES',
  LOAD_MORE_ARTICLES_SUCCESS = 'LOAD_MORE_ARTICLES_SUCCESS',
  LOAD_MORE_ARTICLES_FAILURE = 'LOAD_MORE_ARTICLES_FAILURE',
  GET_ARTICLE_BY_ID = 'GET_ARTICLE_BY_ID',
  GET_ARTICLE_BY_ID_SUCCESS = 'GET_ARTICLE_BY_ID_SUCCESS',
  GET_ARTICLE_BY_ID_FAILURE = 'GET_ARTICLE_BY_ID_FAILURE',
  RESET_SEARCH_RESULTS = 'RESET_SEARCH_RESULTS',
}

export interface INewsActionPayload {
  type: NewsActions
  data?: string | number | IArticle | IArticle[] | { message: string }
}

// Action creators
export const searchArticles = (data: string): INewsActionPayload => ({
  type: NewsActions.SEARCH_ARTICLES,
  data,
})

export const searchArticlesSuccess = (
  data: IArticle[]
): INewsActionPayload => ({
  type: NewsActions.SEARCH_ARTICLES_SUCCESS,
  data,
})

export const searchArticlesFailure = (data: {
  message: string
}): INewsActionPayload => ({
  type: NewsActions.SEARCH_ARTICLES_FAILURE,
  data,
})

export const loadMoreArticles = (data: number): INewsActionPayload => ({
  type: NewsActions.LOAD_MORE_ARTICLES,
  data,
})

export const loadMoreArticlesSuccess = (
  data: IArticle[]
): INewsActionPayload => ({
  type: NewsActions.LOAD_MORE_ARTICLES_SUCCESS,
  data,
})

export const loadMoreArticlesFailure = (data: {
  message: string
}): INewsActionPayload => ({
  type: NewsActions.LOAD_MORE_ARTICLES_FAILURE,
  data,
})

export const getArticleById = (data: string): INewsActionPayload => ({
  type: NewsActions.GET_ARTICLE_BY_ID,
  data,
})

export const getArticleByIdSuccess = (data: IArticle): INewsActionPayload => ({
  type: NewsActions.GET_ARTICLE_BY_ID_SUCCESS,
  data,
})

export const getArticleByIdFailure = (data: {
  message: string
}): INewsActionPayload => ({
  type: NewsActions.GET_ARTICLE_BY_ID_FAILURE,
  data,
})

export const resetSearchResults = (): INewsActionPayload => ({
  type: NewsActions.RESET_SEARCH_RESULTS,
})
