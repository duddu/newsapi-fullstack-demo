import { INewsActionPayload, NewsActions } from './actions'
import { IArticle } from 'server/src/services/search-news'

export interface INewsState {
  search: {
    text?: string
    id?: string
    page?: number
  }
  isLoading: boolean
  news: IArticle[] | null
  error: {
    message: string
  } | null
}

export const initialState: INewsState = {
  search: {},
  isLoading: false,
  news: null,
  error: null,
}

export const reducer = (
  state: INewsState,
  { type, data }: INewsActionPayload
): INewsState => {
  switch (type) {
    case NewsActions.SEARCH_ARTICLES:
      if (data === state.search.text) {
        return state
      }
      return {
        ...state,
        isLoading: true,
        search: {
          text: data as string,
        },
      }
    case NewsActions.SEARCH_ARTICLES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        news: data as IArticle[],
        error: null,
      }
    case NewsActions.SEARCH_ARTICLES_FAILURE:
    case NewsActions.LOAD_MORE_ARTICLES_FAILURE:
    case NewsActions.GET_ARTICLE_BY_ID_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: data as { message: string },
      }
    case NewsActions.GET_ARTICLE_BY_ID:
      return {
        ...state,
        isLoading: true,
        search: {
          ...state.search,
          ...{
            id: data as string,
          },
        },
      }
    case NewsActions.GET_ARTICLE_BY_ID_SUCCESS:
      return {
        ...state,
        isLoading: false,
        news: (state.news || []).concat(data as IArticle),
        error: null,
      }
    case NewsActions.LOAD_MORE_ARTICLES:
      return {
        ...state,
        isLoading: true,
        search: {
          ...state.search,
          page: data as number,
        },
      }
    case NewsActions.LOAD_MORE_ARTICLES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        news: (state.news || []).concat(data as IArticle[]),
        error: null,
      }
    case NewsActions.RESET_SEARCH_RESULTS:
      return {
        ...state,
        news: [],
      }
    default:
      return state
  }
}
