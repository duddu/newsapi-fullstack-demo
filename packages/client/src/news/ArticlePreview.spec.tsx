import React from 'react'
import { createMemoryHistory } from 'history'
import { Router } from 'react-router-dom'
import { act, cleanup, fireEvent, render } from '@testing-library/react'

import { ArticlePreview, getHostname } from './ArticlePreview'
import { _articleMock } from './__mocks__/request'

afterEach(cleanup)

describe.only('[ArticlePreview]', () => {
  const history = createMemoryHistory()
  const article = (
    <Router history={history}>
      <ArticlePreview {..._articleMock} />
    </Router>
  )

  it('should render without crashing', () => {
    const { unmount } = render(article)
    expect(unmount()).toBe(true)
  })

  it('should link to the article detail route', () => {
    const { getByTestId } = render(article)
    expect(getByTestId('link').getAttribute('href')).toBe(
      '/id-mock'
    )
  })

  it('should render the article image', () => {
    const { getByTitle } = render(article)
    expect(getByTitle('title-mock').style.backgroundImage).toBe(
      'url(urlToImage-mock)'
    )
  })

  it('should render the article title', () => {
    const { getByTestId } = render(article)
    expect(getByTestId('title').textContent).toBe('title-mock')
  })

  it('should render the article hostname', () => {
    const { getByTestId } = render(article)
    expect(getByTestId('hostname').textContent).toBeDefined()
  })

  it('should go to /:newsId if inner element clicked', () => {
    const { getByTestId } = render(article)
    act(() => {
      fireEvent.click(getByTestId('title'))
    })
    expect(history.location.pathname).toBe('/id-mock')
  })

  describe('getHostname()', () => {
    it('should return the hostname of a valid url', () => {
      expect(getHostname('http://domain:1234')).toBe('domain')
    })

    it('should return null if no url is provided', () => {
      expect(getHostname(undefined as any)).toBe(null)
    })

    it('should return null if invalid url is provided', () => {
      expect(getHostname('invalid-url')).toBe(null)
    })
  })
})
