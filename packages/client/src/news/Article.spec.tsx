import React from 'react'
import { createMemoryHistory } from 'history'
import { Route, Router } from 'react-router-dom'
import {
  cleanup,
  render,
  waitForDomChange
} from '@testing-library/react'

import { Article } from './Article'
import Provider from './Provider'

jest.mock('./request')

afterEach(cleanup)

describe('[Article]', () => {
  const history = createMemoryHistory({initialEntries: ['/id-mock']});
  const article = (
    <Router history={history}>
      <Article />
    </Router>
  )
  const articleWithProvider = (
    <Router history={history}>
      <Provider>
        <Route path="/:newsId">
          <Article />
        </Route>
      </Provider>
    </Router>
  )

  it('should render without crashing', () => {
    const { unmount } = render(article)
    expect(unmount()).toBe(true)
  })

  it('should be empty if no context provided', () => {
    const { container } = render(article)
    expect(container.textContent).toBe('')
  })

  it('should render the article image', async () => {
    const { getByTestId } = render(articleWithProvider)
    await waitForDomChange()
    expect(getByTestId('image').getAttribute('src')).toBe('urlToImage-mock')
    expect(getByTestId('image').getAttribute('alt')).toBe('title-mock')
  })

  it('should render the article publish date', async () => {
    const { getByTestId } = render(articleWithProvider)
    await waitForDomChange()
    expect(Date.parse(getByTestId('date').textContent as string)).not.toBe(NaN)
  })

  it('should render the article title', async () => {
    const { getByTestId } = render(articleWithProvider)
    await waitForDomChange()
    expect(getByTestId('title').textContent).toBe('title-mock')
  })

  it('should render the article author', async () => {
    const { getByTestId } = render(articleWithProvider)
    await waitForDomChange()
    expect(getByTestId('author').textContent).toBe('author-mock')
  })

  it('should render the article description', async () => {
    const { getByTestId } = render(articleWithProvider)
    await waitForDomChange()
    expect(getByTestId('description').textContent).toBe('description-mock')
  })

  it('should render the read full article button', async () => {
    const { getByTestId } = render(articleWithProvider)
    await waitForDomChange()
    expect(getByTestId('link').textContent).toMatch(/read the full article/i)
  })
})
