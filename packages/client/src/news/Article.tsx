import React, { useContext, useEffect, useLayoutEffect, useRef, useState } from 'react'
import { useParams } from 'react-router-dom'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import { Feedback } from './Feedback'
import { Context } from './Provider'
import { NewsActions } from './actions'
import { IArticle } from 'server/src/services/search-news'

const useStyles = makeStyles(() =>
  createStyles({
    img: {
      width: '100%',
      position: 'fixed',
      top: 0,
      zIndex: -1,
    },
    content: {
      padding: 20,
      background: 'white',
    },
    button: {
      marginTop: 20,
      marginBottom: 30,
    },
    link: {
      textDecoration: 'none',
      color: 'white',
    },
  })
)

/**
 * Displays a single article, supporting deeplinking (e.g. page refresh)
 */
export const Article: React.FC = () => {
  const classes = useStyles()
  const imgRef = useRef<HTMLImageElement>(null)
  const [contentMargin, setContentMargin] = useState<number>()
  const [article, setArticle] = useState<IArticle>()
  const { news, dispatch } = useContext(Context)
  const { newsId } = useParams()

  // This effect checks wheter an article with the id present in the url param
  // is already present in the context state, otherwise dispatches an action
  // to get it fresh from the server.
  useEffect(() => {
    if (!newsId) return
    const article = news && news.find(({ id }) => id === newsId)
    if (article) {
      setArticle(article)
    } else {
      dispatch({
        type: NewsActions.GET_ARTICLE_BY_ID,
        data: newsId,
      })
    }
  }, [newsId, news, dispatch])

  // Set the margin of the content container to be
  // equal to the height of the article image
  const doSetContentMargin = () => {
    if (!imgRef.current) return
    setContentMargin(imgRef.current.height)
  }

  // This effects set a listener on the window resize event,
  // to update the margin of the content container every time
  // the article image changes in height
  useLayoutEffect(() => {
    window.addEventListener('resize', doSetContentMargin);
    return () => window.removeEventListener('resize', doSetContentMargin);
  }, []);

  return (
    <React.Fragment>
      <Feedback />
      {article && (
        <React.Fragment>
          <img
            className={classes.img}
            ref={imgRef}
            src={article.urlToImage}
            onLoad={doSetContentMargin}
            alt={article.title}
            data-testid='image'
          />
          <div className={classes.content} style={{marginTop: contentMargin}}>
            <Typography
              gutterBottom
              variant="body2"
              color="textSecondary"
              component="p"
              data-testid='date'
            >
              {new Date(article.publishedAt).toLocaleDateString()}
            </Typography>
            <Typography
              gutterBottom
              variant="h4"
              component="h1"
              data-testid="title"
            >
              {article.title}
            </Typography>
            <Typography
              gutterBottom
              variant="body2"
              color="textSecondary"
              component="p"
              data-testid="author"
            >
              {article.author}
            </Typography>
            <Typography component="p" data-testid="description">{article.description}</Typography>
            <a
              href={article.url}
              title="Read the full article"
              className={classes.link}
            >
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                data-testid="link"
              >
                Read the full article
              </Button>
            </a>
          </div>
        </React.Fragment>
      )}
    </React.Fragment>
  )
}
