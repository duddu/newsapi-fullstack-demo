import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'
import { cleanup, render, waitForDomChange, findByTestId } from '@testing-library/react'

import { ArticlesList } from './ArticlesList'
import Provider from './Provider'

jest.mock('./request')

afterEach(cleanup)

describe('[ArticlesList]', () => {
  const list = <Provider><Router><ArticlesList /></Router></Provider>

  it('should render without crashing', async () => {
    const { unmount, debug } = render(list)
    await waitForDomChange()
    expect(unmount()).toBe(true)
  })

  it('should render search form', async () => {
    const { getByTestId } = render(list)
    await waitForDomChange()
    expect(getByTestId('form')).toBeDefined()
  })

  it('should render three mock articles', async () => {
    const { getAllByTestId } = render(list)
    await waitForDomChange()
    expect(getAllByTestId('title')).toHaveLength(3)
  })
})
