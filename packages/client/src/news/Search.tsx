import React, { useContext, useEffect, useState } from 'react'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'

import { NewsActions } from './actions'
import { Context } from './Provider'

/**
 * The initial keyword to use as search query
 */
export const initialSearchText = 'london'
const minSearchChars = 3

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2px 4px',
      display: 'flex',
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
  })
)

export const Search: React.FC = () => {
  const classes = useStyles()
  const { search, dispatch } = useContext(Context)
  const [value, setValue] = useState(search.text || initialSearchText)
  const [searchText, setSearchText] = useState(value)

  // This effect updates the context search state every time
  // the local component state is updated
  useEffect(() => {
    if (!searchText) return
    dispatch({
      type: NewsActions.SEARCH_ARTICLES,
      data: searchText
    })
  }, [dispatch, searchText])

  // Update the local component state with the value of the search input
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (!value) return
    setSearchText(value)
  }

  return (
    <form onSubmit={handleSubmit} data-testid="form">
      <Paper className={classes.root}>
        <InputBase
          className={classes.input}
          placeholder="Search powered by News API"
          inputProps={{ 'data-testid': 'search', 'aria-label': 'search news' }}
          value={value}
          onChange={({ target }) => setValue(target.value)}
        />
        <IconButton
          className={classes.iconButton}
          aria-label="search"
          type="submit"
          disabled={value.length < minSearchChars}
        >
          <SearchIcon />
        </IconButton>
      </Paper>
    </form>
  )
}
