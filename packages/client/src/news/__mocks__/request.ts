/** @ignore */
export const _articleMock = {
  id: 'id-mock',
  title: 'title-mock',
  author: 'author-mock',
  description: 'description-mock',
  url: 'http://host-mock:9999/path',
  urlToImage: 'urlToImage-mock',
  publishedAt: new Date(),
}

/** @ignore */
export const fetchArticle = () =>
  Promise.resolve({
    data: _articleMock,
  })

/** @ignore */
export const fetchArticles = () =>
  Promise.resolve({
    data: Array(3)
      .fill(_articleMock)
      .map((mock, i) => ({
        ...mock,
        id: `${mock.id}_${i}`,
      })),
  })
